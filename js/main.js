'use strict'

/* Services */

const servicesHeader = document.querySelector('.services-title'); 
const servicesContent = document.querySelectorAll('.services-content');

servicesHeader.addEventListener('click',(event)=>{ 

  const servicesTitles = servicesHeader.querySelectorAll('.services-title-items');

  servicesTitles.forEach(e => e.classList.remove('services-active'));

  event.target.classList.add('services-active'); 

  servicesContent.forEach(e => { 

    e.dataset.content === event.target.dataset.title ? e.style.display='flex' : e.style.display='none' 

  });

}); 

/* Works */

const imgSrcArr = [
  {
    category: 'Web Design',
    images: [
      './img/web-design/web-design1.jpg',
      './img/web-design/web-design2.jpg',
      './img/web-design/web-design3.jpg',
      './img/web-design/web-design4.jpg',
      './img/web-design/web-design5.jpg',
      './img/web-design/web-design6.jpg',
      './img/web-design/web-design7.jpg',
      './img/web-design/web-design8.jpg',
      './img/web-design/web-design9.jpg',
      './img/web-design/web-design10.jpg',
      './img/web-design/web-design11.jpg',
      './img/web-design/web-design12.jpg',
      './img/web-design/web-design13.jpg',
      './img/web-design/web-design14.jpg',

    ],
  },
  {
    category: 'Graphic Design',
    images: [
      './img/graphic-design/graphic-design2.jpg',
      './img/graphic-design/graphic-design2.jpg',
      './img/graphic-design/graphic-design3.jpg',
      './img/graphic-design/graphic-design4.jpg',
      './img/graphic-design/graphic-design5.jpg',
      './img/graphic-design/graphic-design6.jpg',
      './img/graphic-design/graphic-design7.jpg',
      './img/graphic-design/graphic-design8.jpg',
      './img/graphic-design/graphic-design9.jpg',
      './img/graphic-design/graphic-design10.jpg',
      './img/graphic-design/graphic-design11.jpg',
      './img/graphic-design/graphic-design12.jpg',
      './img/graphic-design/graphic-design13.jpg',

    ],
  },
  {
    category: 'Landing Pages',
    images: [
      './img/landing-page/landing-page1.jpg',
      './img/landing-page/landing-page2.jpg',
      './img/landing-page/landing-page3.jpg',
      './img/landing-page/landing-page4.jpg',
      './img/landing-page/landing-page5.jpg',
      './img/landing-page/landing-page6.jpg',
      './img/landing-page/landing-page7.jpg',
      './img/landing-page/landing-page8.jpg',
      './img/landing-page/landing-page9.jpg',
      './img/landing-page/landing-page10.jpg',
      './img/landing-page/landing-page11.jpg',
      './img/landing-page/landing-page12.jpg',
      './img/landing-page/landing-page13.jpg',
    ],
  },
  {
    category: 'Wordpress',
    images: [
      './img/wordpress/wordpress1.jpg',
      './img/wordpress/wordpress2.jpg',
      './img/wordpress/wordpress3.jpg',
      './img/wordpress/wordpress4.jpg',
      './img/wordpress/wordpress5.jpg',
      './img/wordpress/wordpress6.jpg',
      './img/wordpress/wordpress7.jpg',
      './img/wordpress/wordpress8.jpg',
      './img/wordpress/wordpress9.jpg',
      './img/wordpress/wordpress10.jpg',
      './img/wordpress/wordpress11.jpg',
      './img/wordpress/wordpress12.jpg',
      './img/wordpress/wordpress13.jpg',
      './img/wordpress/wordpress14.jpg',
      './img/wordpress/wordpress15.jpg',

    ],
  },
];

const ourWorksTabs = document.querySelector('.works-title');
const imgArrayContainer = document.querySelector('.works-img-container');

ourWorksTabs.addEventListener('click', sortCategoryBy);

function getSortedImagesByCategory(category) {
  let count = 0;
  let sortedImagesByCategory = [];
  imgSrcArr.forEach((e) => {
    if (category === e.category) {
      sortedImagesByCategory = e.images.map((imageSrc) => {
        return `
            <div class="flip-card" data-img-index=${count++}>
              <div class="flip-card-inner">
              <div class="flip-card-front">
              <img src=${imageSrc} data-category="${e.category}" alt="${e.category}"/>
            </div>
            <div class="flip-card-back">
              <div class="flip-card-back-img-container"> 
                <img src="./img/5persons/icons/Group-5.svg"/>
                <img src="./img/5persons/icons/Group-6.svg"/>
              </div>
              <p>CREATIVE DESIGN</p> 
              <h1>${e.category}</h1> 
            </div>
          </div>
        </div>`;
      });
    }
  });

  return sortedImagesByCategory;
}

function sortCategoryBy(e) {

  const li = e.target.closest('li');
  if (!li) {return}

  const activeSortTab = document.querySelector('.active-sort-tab');

  if (activeSortTab) {activeSortTab.classList.remove('active-sort-tab');}

  loadMoreBtn.classList.remove('load-more-btn-hidden');
  li.classList.add('active-sort-tab');
  imgArrayContainer.innerHTML = '';

  if (li.dataset.category === 'All') {
    const allImages = getAllImages();
    const cutImagesArr = allImages.filter((i, index) => index < 12);
    imgArrayContainer.insertAdjacentHTML('afterbegin', cutImagesArr.join(''));  
  
  } else if (li.dataset.category === 'Web Design') {
    const allImages = getSelectedImages('Web Design');

  } else if (li.dataset.category === 'Graphic Design') {
    const allImages = getSelectedImages('Graphic Design');

  } else if (li.dataset.category === 'Landing Pages') {
    const allImages = getSelectedImages('Landing Pages');

  } else if (li.dataset.category === 'Wordpress') {
    const allImages = getSelectedImages('Wordpress');

  } else {
    const sortedArr = getSortedImagesByCategory(li.dataset.category);

    if (sortedArr.length <= 12) {loadMoreBtn.classList.add('load-more-btn-hidden');}
    imgArrayContainer.insertAdjacentHTML('afterbegin', sortedArr.join(''));
  }

  
}

function getSelectedImages(img) {

  const filteredImages = imgSrcArr.filter(obj => obj.category === img);
  
  let count = 0;

  const allImages = filteredImages.map((e) => {
    return e.images.map((filteredImages) => {
      return `
          <div class="flip-card" data-img-index=${count++}>
          <div class="flip-card-inner">
            <div class="flip-card-front">
            <img src=${filteredImages} data-category="${e.category}" alt="${e.category}"/>
            </div>
            <div class="flip-card-back">
              <div class="flip-card-back-img-container"> 
                <img src="./img/5persons/icons/Group-5.svg"/>
                <img src="./img/5persons/icons/Group-6.svg"/>
              </div>
              <p>CREATIVE DESIGN</p> 
              <h1>${e.category}</h1> 
            </div>
          </div>
        </div>
      `;
    });
  });

  const arr = [];

  allImages.forEach((elem) => {
    arr.push(...elem);
  });

  return arr;
}

function getAllImages() {

  let count = 0;

  const allImages = imgSrcArr.map((e) => {
    return e.images.map((imageSrc) => {
      return `
          <div class="flip-card" data-img-index=${count++}>
          <div class="flip-card-inner">
            <div class="flip-card-front">
            <img src=${imageSrc} data-category="${e.category}" alt="${e.category}"/>
            </div>
            <div class="flip-card-back">
              <div class="flip-card-back-img-container"> 
                <img src="./img/5persons/icons/Group-5.svg"/>
                <img src="./img/5persons/icons/Group-6.svg"/>
              </div>
              <p>CREATIVE DESIGN</p> 
              <h1>${e.category}</h1> 
            </div>
          </div>
        </div>
      `;
    });
  });

  const arr = [];

  allImages.forEach((elem) => {
    arr.push(...elem);
  });

  return arr;
}


const activeAllSortTab = document.querySelector('.works-title-items');

const worksHeader = document.querySelector('.works-title'); 

let allImages;

if (activeAllSortTab.dataset.category === 'All') {
  allImages = getAllImages();
  const cutImagesArr = allImages.filter((i, index) => index < 12);
  imgArrayContainer.insertAdjacentHTML('afterbegin', cutImagesArr.join(''));
} 
worksHeader.addEventListener('click',(event)=>{

  
  if (event.target.dataset.im === 'Web Design') {
  allImages = getSelectedImages(event.target.dataset.category);
  const cutImagesArr = allImages.filter((i, index) => index < 12);
  imgArrayContainer.insertAdjacentHTML('afterbegin', cutImagesArr.join(''));

  } else if (event.target.dataset.category === 'Graphic Design') {
    allImages = getSelectedImages(event.target.dataset.category);
    const cutImagesArr = allImages.filter((i, index) => index < 12);
    imgArrayContainer.insertAdjacentHTML('afterbegin', cutImagesArr.join(''));
  
  } else if (event.target.dataset.category === 'Landing Pages') {
  allImages = getSelectedImages(event.target.dataset.category);
  const cutImagesArr = allImages.filter((i, index) => index < 12);
  imgArrayContainer.insertAdjacentHTML('afterbegin', cutImagesArr.join(''));

  } else if (event.target.dataset.category === 'Wordpress') {
    allImages = getSelectedImages(event.target.dataset.category);
    const cutImagesArr = allImages.filter((i, index) => index < 12);
    imgArrayContainer.insertAdjacentHTML('afterbegin', cutImagesArr.join(''));
  
  }
  




}); 






activeAllSortTab.classList.add('active-sort-tab');
const loadMoreBtn = document.querySelector('.load-more-btn');
loadMoreBtn.addEventListener('click', addMoreImages);

function addMoreImages() {

  const loadMoreBtn = document.querySelector('.load-more-btn');
  loadMoreBtn.innerHTML = '<i class="fa load-more-btn-icon fa-spinner fa-spin"></i> <span>Loading</span>';
  const ourWorksImgContainer = document.querySelector('.works-img-container');
  const ourWorksImgLastChild = ourWorksImgContainer.lastElementChild;
  console.log(ourWorksImgContainer);

  setTimeout(() => {

    if (ourWorksImgLastChild.dataset.imgIndex === '23') {
      const thirdArr = allImages.slice(24, 36);
      imgArrayContainer.insertAdjacentHTML('beforeend', thirdArr.join(''));
      loadMoreBtn.innerHTML = `
        <img class="load-more-btn-icon" src="./img/5persons/icons/Forma 1.svg" alt="plus" />
        <span>LOAD MORE</span>
      `;
      loadMoreBtn.classList.add('load-more-btn-hidden');
    }

    if (ourWorksImgLastChild.dataset.imgIndex === '11') {
      const secondArr = allImages.slice(12, 24);
      imgArrayContainer.insertAdjacentHTML('beforeend', secondArr.join(''));
      loadMoreBtn.innerHTML = `
      <img class="load-more-btn-icon" src="./img/5persons/icons/Forma 1.svg" alt="plus" />
      <span>LOAD MORE</span>
      `;
    }
  }, 1000);
}


/* Slider */

const bulletIconContainer = document.querySelector('.bullet-icons-container');
const bulletIconItems = document.querySelectorAll('.bullet-icons-item');

const swiperSlideContainer = document.querySelectorAll('.swiper-slide');

bulletIconItems[0].classList.add('swiper-icon-active');

bulletIconContainer.addEventListener('click', swiperContentDisplay);

const swiperSlideFirstItem = document.querySelector('.swiper-slide');
const bulletIconItem = document.querySelector('.bullet-icons-item');

if (bulletIconItem.dataset.article === swiperSlideFirstItem.dataset.article) {
  swiperSlideFirstItem.classList.add('swiper-slide-active');
}

function swiperContentDisplay(e) {

  const selectedPicture = e.target.closest('img');

  if (!selectedPicture) {return;}

  const activeSwiperSlide = document.querySelector('.swiper-icon-active');

  if (activeSwiperSlide) {
    activeSwiperSlide.classList.remove('swiper-icon-active');
  }
  selectedPicture.classList.add('swiper-icon-active');

  swiperSlideContainer.forEach((item) => {
    item.classList.remove('swiper-slide-active');
    if (selectedPicture.dataset.article === item.dataset.article) {
      item.classList.add('swiper-slide-active');
    }
  });
};

const dynamicArrowsContainer = document.querySelector('.dynamic-items-wrapper');

const rightArrow = document.querySelector('.swiper-button-right');
const leftArrow = document.querySelector('.swiper-button-left');

dynamicArrowsContainer.addEventListener('click', dynamicArrows);

function dynamicArrows(e) {

  const arrow = e.target.closest('div');

  for (let index = 0; index <= bulletIconItems.length; index++) {
    const elem = bulletIconItems[index];

    if (arrow.contains(leftArrow) && elem.classList.contains('swiper-icon-active')) {
      
      if (index === 0) {
        index = bulletIconItems.length;
      };
      elem.classList.remove('swiper-icon-active');
      bulletIconItems[index - 1].classList.add('swiper-icon-active');
      break;
    };

    if (bulletIconItems.length - 1 === index) {
      index = -1;
    };

    if (arrow.contains(rightArrow) && elem.classList.contains('swiper-icon-active')) {
      elem.classList.remove('swiper-icon-active');
      bulletIconItems[index + 1].classList.add('swiper-icon-active');
      break;
    };
  };

  swiperSlideContainer.forEach((item) => {
    item.classList.remove('swiper-slide-active');
    bulletIconItems.forEach((elem) => {
      if (elem.classList.contains('swiper-icon-active') && elem.dataset.article === item.dataset.article) {
        item.classList.add('swiper-slide-active');
      };
    });
  });
};